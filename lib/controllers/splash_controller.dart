import 'package:flutter/material.dart';
import 'package:image_app/views/home_view.dart';

class SplashController {
  void startSplashTimer({required BuildContext context}) {
    Future.delayed(
      const Duration(seconds: 4),
      () async {
        Navigator.pushReplacement(
          context,
          MaterialPageRoute(
            builder: (context) => const HomeView(),
          ),
        );
      },
    );
  }
}
