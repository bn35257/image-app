import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:dio/io.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:image_app/api_end_points.dart';
import 'package:image_app/constants.dart';
import 'package:image_app/models/image_response.dart';
import 'package:image_app/views/detail_view.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';

class HomeController {
  final baseUrl = EndPoint.baseUrl;
  Dio? client;
  Future<List<ImageResponse>>? imageList;
  Response<dynamic>? response;

  Future<List<ImageResponse>> callImageApi(BuildContext context) async {
    initializeNetworkClient();
    try {
      response = await client?.get(
        EndPoint.getAllImage,
        queryParameters: {
          'client_id': Constant.CLIENT_ID,
        },
      );
    } catch (e) {
      debugPrint('${Constant.IMAGE_API_CALL}: ${e.toString()}');
    }
    return imageResponseFromJson(jsonEncode(response?.data));
  }

  void initializeNetworkClient() {
    client = Dio(
      BaseOptions(
        connectTimeout: const Duration(seconds: Constant.MAX_TIMEOUT),
        receiveTimeout: const Duration(seconds: Constant.MAX_TIMEOUT),
        baseUrl: baseUrl,
      ),
    );

    client?.httpClientAdapter = IOHttpClientAdapter(
      createHttpClient: () {
        HttpClient hClient = HttpClient();
        hClient.badCertificateCallback = (cert, host, port) => true;
        return hClient;
      },
    );

    client?.interceptors.add(
      PrettyDioLogger(
        maxWidth: Constant.MAX_LOG_WIDTH,
        request: kDebugMode,
        requestBody: kDebugMode,
        requestHeader: kDebugMode,
        responseBody: kDebugMode,
        responseHeader: kDebugMode,
      ),
    );
  }

  void onClickImage(
      {String? imageUrl,
      String? description,
      String? likes,
      String? id,
      required BuildContext context}) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => DetailView(
          description: description,
          url: imageUrl,
          likes: likes,
          id: id,
        ),
      ),
    );
  }
}
