// To parse this JSON data, do
//
//     final imageResponse = imageResponseFromJson(jsonString);

import 'dart:convert';

List<ImageResponse> imageResponseFromJson(String str) =>
    List<ImageResponse>.from(
        json.decode(str).map((x) => ImageResponse.fromJson(x)));

String imageResponseToJson(List<ImageResponse> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class ImageResponse {
  ImageResponse({
    this.id,
    this.slug,
    this.createdAt,
    this.updatedAt,
    this.promotedAt,
    this.width,
    this.height,
    this.color,
    this.blurHash,
    this.description,
    this.altDescription,
    this.breadcrumbs,
    this.urls,
    this.links,
    this.likes,
    this.likedByUser,
    this.currentUserCollections,
    this.sponsorship,
    this.topicSubmissions,
    this.user,
  });

  factory ImageResponse.fromJson(Map<String, dynamic> json) => ImageResponse(
        id: json["id"],
        slug: json["slug"],
        createdAt: json["created_at"] == null
            ? null
            : DateTime.parse(json["created_at"]),
        updatedAt: json["updated_at"] == null
            ? null
            : DateTime.parse(json["updated_at"]),
        promotedAt: json["promoted_at"] == null
            ? null
            : DateTime.parse(json["promoted_at"]),
        width: json["width"],
        height: json["height"],
        color: json["color"],
        blurHash: json["blur_hash"],
        description: json["description"],
        altDescription: json["alt_description"],
        breadcrumbs: json["breadcrumbs"] == null
            ? []
            : List<dynamic>.from(json["breadcrumbs"]!.map((x) => x)),
        urls: json["urls"] == null ? null : Urls.fromJson(json["urls"]),
        links: json["links"] == null
            ? null
            : ImageResponseLinks.fromJson(json["links"]),
        likes: json["likes"],
        likedByUser: json["liked_by_user"],
        currentUserCollections: json["current_user_collections"] == null
            ? []
            : List<dynamic>.from(
                json["current_user_collections"]!.map((x) => x)),
        sponsorship: json["sponsorship"] == null
            ? null
            : Sponsorship.fromJson(json["sponsorship"]),
        topicSubmissions: json["topic_submissions"] == null
            ? null
            : TopicSubmissions.fromJson(json["topic_submissions"]),
        user: json["user"] == null ? null : User.fromJson(json["user"]),
      );

  final String? altDescription;
  final String? blurHash;
  final List<dynamic>? breadcrumbs;
  final String? color;
  final DateTime? createdAt;
  final List<dynamic>? currentUserCollections;
  final String? description;
  final int? height;
  final String? id;
  final bool? likedByUser;
  final int? likes;
  final ImageResponseLinks? links;
  final DateTime? promotedAt;
  final String? slug;
  final Sponsorship? sponsorship;
  final TopicSubmissions? topicSubmissions;
  final DateTime? updatedAt;
  final Urls? urls;
  final User? user;
  final int? width;

  Map<String, dynamic> toJson() => {
        "id": id,
        "slug": slug,
        "created_at": createdAt?.toIso8601String(),
        "updated_at": updatedAt?.toIso8601String(),
        "promoted_at": promotedAt?.toIso8601String(),
        "width": width,
        "height": height,
        "color": color,
        "blur_hash": blurHash,
        "description": description,
        "alt_description": altDescription,
        "breadcrumbs": breadcrumbs == null
            ? []
            : List<dynamic>.from(breadcrumbs!.map((x) => x)),
        "urls": urls?.toJson(),
        "links": links?.toJson(),
        "likes": likes,
        "liked_by_user": likedByUser,
        "current_user_collections": currentUserCollections == null
            ? []
            : List<dynamic>.from(currentUserCollections!.map((x) => x)),
        "sponsorship": sponsorship?.toJson(),
        "topic_submissions": topicSubmissions?.toJson(),
        "user": user?.toJson(),
      };
}

class ImageResponseLinks {
  ImageResponseLinks({
    this.self,
    this.html,
    this.download,
    this.downloadLocation,
  });

  factory ImageResponseLinks.fromJson(Map<String, dynamic> json) =>
      ImageResponseLinks(
        self: json["self"],
        html: json["html"],
        download: json["download"],
        downloadLocation: json["download_location"],
      );

  final String? download;
  final String? downloadLocation;
  final String? html;
  final String? self;

  Map<String, dynamic> toJson() => {
        "self": self,
        "html": html,
        "download": download,
        "download_location": downloadLocation,
      };
}

class Sponsorship {
  Sponsorship({
    this.impressionUrls,
    this.tagline,
    this.taglineUrl,
    this.sponsor,
  });

  factory Sponsorship.fromJson(Map<String, dynamic> json) => Sponsorship(
        impressionUrls: json["impression_urls"] == null
            ? []
            : List<String>.from(json["impression_urls"]!.map((x) => x)),
        tagline: json["tagline"],
        taglineUrl: json["tagline_url"],
        sponsor:
            json["sponsor"] == null ? null : User.fromJson(json["sponsor"]),
      );

  final List<String>? impressionUrls;
  final User? sponsor;
  final String? tagline;
  final String? taglineUrl;

  Map<String, dynamic> toJson() => {
        "impression_urls": impressionUrls == null
            ? []
            : List<dynamic>.from(impressionUrls!.map((x) => x)),
        "tagline": tagline,
        "tagline_url": taglineUrl,
        "sponsor": sponsor?.toJson(),
      };
}

class User {
  User({
    this.id,
    this.updatedAt,
    this.username,
    this.name,
    this.firstName,
    this.lastName,
    this.twitterUsername,
    this.portfolioUrl,
    this.bio,
    this.location,
    this.links,
    this.profileImage,
    this.instagramUsername,
    this.totalCollections,
    this.totalLikes,
    this.totalPhotos,
    this.totalPromotedPhotos,
    this.acceptedTos,
    this.forHire,
    this.social,
  });

  factory User.fromJson(Map<String, dynamic> json) => User(
        id: json["id"],
        updatedAt: json["updated_at"] == null
            ? null
            : DateTime.parse(json["updated_at"]),
        username: json["username"],
        name: json["name"],
        firstName: json["first_name"],
        lastName: json["last_name"],
        twitterUsername: json["twitter_username"],
        portfolioUrl: json["portfolio_url"],
        bio: json["bio"],
        location: json["location"],
        links: json["links"] == null ? null : UserLinks.fromJson(json["links"]),
        profileImage: json["profile_image"] == null
            ? null
            : ProfileImage.fromJson(json["profile_image"]),
        instagramUsername: json["instagram_username"],
        totalCollections: json["total_collections"],
        totalLikes: json["total_likes"],
        totalPhotos: json["total_photos"],
        totalPromotedPhotos: json["total_promoted_photos"],
        acceptedTos: json["accepted_tos"],
        forHire: json["for_hire"],
        social: json["social"] == null ? null : Social.fromJson(json["social"]),
      );

  final bool? acceptedTos;
  final String? bio;
  final String? firstName;
  final bool? forHire;
  final String? id;
  final String? instagramUsername;
  final String? lastName;
  final UserLinks? links;
  final String? location;
  final String? name;
  final String? portfolioUrl;
  final ProfileImage? profileImage;
  final Social? social;
  final int? totalCollections;
  final int? totalLikes;
  final int? totalPhotos;
  final int? totalPromotedPhotos;
  final String? twitterUsername;
  final DateTime? updatedAt;
  final String? username;

  Map<String, dynamic> toJson() => {
        "id": id,
        "updated_at": updatedAt?.toIso8601String(),
        "username": username,
        "name": name,
        "first_name": firstName,
        "last_name": lastName,
        "twitter_username": twitterUsername,
        "portfolio_url": portfolioUrl,
        "bio": bio,
        "location": location,
        "links": links?.toJson(),
        "profile_image": profileImage?.toJson(),
        "instagram_username": instagramUsername,
        "total_collections": totalCollections,
        "total_likes": totalLikes,
        "total_photos": totalPhotos,
        "total_promoted_photos": totalPromotedPhotos,
        "accepted_tos": acceptedTos,
        "for_hire": forHire,
        "social": social?.toJson(),
      };
}

class UserLinks {
  UserLinks({
    this.self,
    this.html,
    this.photos,
    this.likes,
    this.portfolio,
    this.following,
    this.followers,
  });

  factory UserLinks.fromJson(Map<String, dynamic> json) => UserLinks(
        self: json["self"],
        html: json["html"],
        photos: json["photos"],
        likes: json["likes"],
        portfolio: json["portfolio"],
        following: json["following"],
        followers: json["followers"],
      );

  final String? followers;
  final String? following;
  final String? html;
  final String? likes;
  final String? photos;
  final String? portfolio;
  final String? self;

  Map<String, dynamic> toJson() => {
        "self": self,
        "html": html,
        "photos": photos,
        "likes": likes,
        "portfolio": portfolio,
        "following": following,
        "followers": followers,
      };
}

class ProfileImage {
  ProfileImage({
    this.small,
    this.medium,
    this.large,
  });

  factory ProfileImage.fromJson(Map<String, dynamic> json) => ProfileImage(
        small: json["small"],
        medium: json["medium"],
        large: json["large"],
      );

  final String? large;
  final String? medium;
  final String? small;

  Map<String, dynamic> toJson() => {
        "small": small,
        "medium": medium,
        "large": large,
      };
}

class Social {
  Social({
    this.instagramUsername,
    this.portfolioUrl,
    this.twitterUsername,
    this.paypalEmail,
  });

  factory Social.fromJson(Map<String, dynamic> json) => Social(
        instagramUsername: json["instagram_username"],
        portfolioUrl: json["portfolio_url"],
        twitterUsername: json["twitter_username"],
        paypalEmail: json["paypal_email"],
      );

  final String? instagramUsername;
  final dynamic paypalEmail;
  final String? portfolioUrl;
  final String? twitterUsername;

  Map<String, dynamic> toJson() => {
        "instagram_username": instagramUsername,
        "portfolio_url": portfolioUrl,
        "twitter_username": twitterUsername,
        "paypal_email": paypalEmail,
      };
}

class TopicSubmissions {
  TopicSubmissions({
    this.travel,
    this.coolTones,
    this.film,
    this.sports,
    this.nature,
    this.wallpapers,
  });

  factory TopicSubmissions.fromJson(Map<String, dynamic> json) =>
      TopicSubmissions(
        travel:
            json["travel"] == null ? null : CoolTones.fromJson(json["travel"]),
        coolTones: json["cool-tones"] == null
            ? null
            : CoolTones.fromJson(json["cool-tones"]),
        film: json["film"] == null ? null : CoolTones.fromJson(json["film"]),
        sports: json["sports"] == null ? null : Nature.fromJson(json["sports"]),
        nature: json["nature"] == null ? null : Nature.fromJson(json["nature"]),
        wallpapers: json["wallpapers"] == null
            ? null
            : Nature.fromJson(json["wallpapers"]),
      );

  final CoolTones? coolTones;
  final CoolTones? film;
  final Nature? nature;
  final Nature? sports;
  final CoolTones? travel;
  final Nature? wallpapers;

  Map<String, dynamic> toJson() => {
        "travel": travel?.toJson(),
        "cool-tones": coolTones?.toJson(),
        "film": film?.toJson(),
        "sports": sports?.toJson(),
        "nature": nature?.toJson(),
        "wallpapers": wallpapers?.toJson(),
      };
}

class CoolTones {
  CoolTones({
    this.status,
    this.approvedOn,
  });

  factory CoolTones.fromJson(Map<String, dynamic> json) => CoolTones(
        status: json["status"],
        approvedOn: json["approved_on"] == null
            ? null
            : DateTime.parse(json["approved_on"]),
      );

  final DateTime? approvedOn;
  final String? status;

  Map<String, dynamic> toJson() => {
        "status": status,
        "approved_on": approvedOn?.toIso8601String(),
      };
}

class Nature {
  Nature({
    this.status,
  });

  factory Nature.fromJson(Map<String, dynamic> json) => Nature(
        status: json["status"],
      );

  final String? status;

  Map<String, dynamic> toJson() => {
        "status": status,
      };
}

class Urls {
  Urls({
    this.raw,
    this.full,
    this.regular,
    this.small,
    this.thumb,
    this.smallS3,
  });

  factory Urls.fromJson(Map<String, dynamic> json) => Urls(
        raw: json["raw"],
        full: json["full"],
        regular: json["regular"],
        small: json["small"],
        thumb: json["thumb"],
        smallS3: json["small_s3"],
      );

  final String? full;
  final String? raw;
  final String? regular;
  final String? small;
  final String? smallS3;
  final String? thumb;

  Map<String, dynamic> toJson() => {
        "raw": raw,
        "full": full,
        "regular": regular,
        "small": small,
        "thumb": thumb,
        "small_s3": smallS3,
      };
}
