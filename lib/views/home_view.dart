// ignore_for_file: prefer_const_constructors

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_app/controllers/home_controller.dart';

class HomeView extends StatefulWidget {
  const HomeView({super.key});

  @override
  State<HomeView> createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> {
  final controller = HomeController();

  @override
  void initState() {
    super.initState();
    // homeController.callImageApi();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Home'),
      ),
      body: FutureBuilder(
        future: controller.callImageApi(context),
        builder: (context, snapshot) {
          if (snapshot.data == null) {
            return Center(
              child: CupertinoActivityIndicator(),
            );
          }
          return Scrollbar(
            child: ListView.separated(
                padding: EdgeInsets.symmetric(horizontal: 20),
                physics: BouncingScrollPhysics(
                  decelerationRate: ScrollDecelerationRate.fast,
                ),
                itemBuilder: (context, index) {
                  final data = snapshot.data?[index];
                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        data?.altDescription ?? "N/A",
                        style: TextStyle(
                            fontSize: 20, fontWeight: FontWeight.bold),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      InkWell(
                        onTap: () {
                          controller.onClickImage(
                            context: context,
                            imageUrl: data?.urls?.small,
                            description: data?.description,
                            id: data?.id,
                            likes: data?.likes.toString(),
                          );
                        },
                        child: SizedBox(
                          width: double.infinity,
                          height: 300,
                          child: Hero(
                            tag: 'img-${data?.id}',
                            child: Image.network(
                              loadingBuilder:
                                  (context, child, loadingProgress) {
                                if (loadingProgress == null) {
                                  return child;
                                }
                                return CupertinoActivityIndicator();
                              },
                              data?.urls?.small ?? "",
                            ),
                          ),
                        ),
                      ),
                    ],
                  );
                },
                separatorBuilder: (context, index) => Divider(),
                itemCount: snapshot.data?.length ?? 0),
          );
        },
      ),
    );
  }
}
