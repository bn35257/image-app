// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_app/controllers/detail_controller.dart';

class DetailView extends StatefulWidget {
  const DetailView({
    super.key,
    this.description,
    this.likes,
    this.url,
    this.id,
  });

  final String? description;
  final String? likes;
  final String? url;
  final String? id;

  @override
  State<DetailView> createState() => _DetailViewState();
}

class _DetailViewState extends State<DetailView> {
  final controller = DetailController();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Detail'),
      ),
      body: Column(
        children: [
          SizedBox(
            width: double.infinity,
            height: 300,
            child: Hero(
              tag: 'img-${widget.id}',
              child: Image.network(
                loadingBuilder: (context, child, loadingProgress) {
                  if (loadingProgress == null) {
                    return child;
                  }
                  return CupertinoActivityIndicator();
                },
                widget.url ?? "",
              ),
            ),
          ),
          SizedBox(
            height: 20,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Icon(
                Icons.thumb_up,
                color: Colors.lightBlue,
              ),
              SizedBox(
                width: 10,
              ),
              Text(
                widget.likes ?? "",
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ],
          ),
          SizedBox(
            height: 20,
          ),
          Expanded(
            child: SingleChildScrollView(
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: Text(
                widget.description ?? "N/A",
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 17),
              ),
            ),
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton.extended(
        icon: Icon(Icons.download_rounded),
        onPressed: () async {
          await controller.onClickDownload(
            widget.url ?? "",
            context,
          );
        },
        label: Text('Download'),
      ),
    );
  }
}
