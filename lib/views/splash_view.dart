// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:image_app/constants.dart';
import 'package:image_app/controllers/splash_controller.dart';

class SplashView extends StatefulWidget {
  const SplashView({super.key});

  @override
  State<SplashView> createState() => _SplashViewState();
}

class _SplashViewState extends State<SplashView> {
  final controller = SplashController();

  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback(
      (_) => controller.startSplashTimer(context: context),
    );
    return Scaffold(
      body: SafeArea(
        child: Center(
          child: SizedBox(
            height: 100,
            width: 100,
            child: Image.asset(Constant.APP_ICON),
          ),
        ),
      ),
    );
  }
}
