// ignore_for_file: constant_identifier_names

class Constant {
  static const APP_ICON = "lib/assets/icon.png";
  static const CONNECTION_TIMEOUT = 10;
  static const MAX_LOG_WIDTH = 90;
  static const MAX_TIMEOUT = 90;
  static const CLIENT_ID = "8fZZLAtYG6LkkNbNsa7-_35CESYO-62CM5zQfc_-Nas";
  static const IMAGE_API_CALL = "IMAGE_API_CALL";
}